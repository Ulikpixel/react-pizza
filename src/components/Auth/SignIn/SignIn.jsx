import React from "react";
import { Field, reduxForm } from "redux-form";
import {
  validField,
  validFieldMin,
  validFieldMax,
} from "../../../utils/validField";
import InputField from "../../common/InputField/InputField";
import { signIn } from "../../../redux/actions/auth-actions";
import { useDispatch, useSelector } from "react-redux";

const minLength = validFieldMin(2);
const maxLength = validFieldMax(30);

const Form = ({ handleSubmit, error }) => {
  const loginProgress = useSelector(state => state.auth.loginProgress);
  return (
    <form className="signin__form" onSubmit={handleSubmit}>
      {error && (
        <p className="signin__form--error">
          Вы не правильно ввели свой email или пароль!
        </p>
      )}
      <label className="signin__form--label">Email</label>
      <Field
        component={InputField}
        name="email"
        type="email"
        validate={[validField, maxLength, minLength]}
      />
      <label className="signin__form--label">Пароль</label>
      <Field
        component={InputField}
        name="password"
        type="password"
        validate={[validField, maxLength, minLength]}
      />
      <button className="signin__form--btn" type="submit" disabled={loginProgress}>
        Войти
      </button>
    </form>
  );
};

const SignInForm = reduxForm({ form: "signin" })(Form);

const SignIn = () => {
  const dispatch = useDispatch();
  return (
    <div className="signin">
      <p className="signin__title">Войти</p>
      <SignInForm
        onSubmit={({ email, password }) => dispatch(signIn(email, password))}
      />
    </div>
  );
};

export default SignIn;
