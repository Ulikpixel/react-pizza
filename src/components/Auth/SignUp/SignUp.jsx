import React from "react";
import { Field, reduxForm } from "redux-form";
import InputField from "../../common/InputField/InputField";
import {
  validField,
  validFieldMin,
  validFieldMax,
} from "../../../utils/validField";
import { signUp } from "../../../redux/actions/auth-actions";
import { useDispatch, useSelector } from "react-redux";
const minLength = validFieldMin(2);
const maxLength = validFieldMax(30);

const Form = ({ handleSubmit }) => {
  const loginProgress = useSelector((state) => state.auth.loginProgress);
  return (
    <form className="signup__form" onSubmit={handleSubmit}>
      <label className="signup__form--label">Имя</label>
      <Field
        component={InputField}
        name="name"
        validate={[validField, maxLength, minLength]}
      />
      <label className="signup__form--label">Фамилия</label>
      <Field
        component={InputField}
        name="surname"
        validate={[validField, maxLength, minLength]}
      />
      <label className="signup__form--label">Email</label>
      <Field
        component={InputField}
        name="email"
        type="email"
        validate={[validField, maxLength, minLength]}
      />
      <label className="signup__form--label">Пароль</label>
      <Field
        component={InputField}
        name="password"
        type="password"
        validate={[validField, maxLength, minLength]}
      />
      <button className="signup__form--btn" disabled={loginProgress}>
        Зарегистрироваться
      </button>
    </form>
  );
};

const SignUpForm = reduxForm({ form: "signup" })(Form);

const SignUp = () => {
  const dispatch = useDispatch();
  return (
    <div className="signup">
      <p className="signup__title">Регистрация</p>
      <SignUpForm onSubmit={(data) => dispatch(signUp(data))} />
    </div>
  );
};

export default SignUp;
