import React from "react";
import { NavLink, Route } from "react-router-dom";
import googleImg from "../../assets/img/google.svg";
import telegramImg from "../../assets/img/telegram.svg";
import SignUp from "./SignUp/SignUp";
import SignIn from "./SignIn/SignIn";
import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

const Auth = () => {
  const { isAuth } = useSelector(state => state.auth);
  if(isAuth) return <Redirect to="/basket" />
  return (
    <section className="auth">
      <div className="auth__container">
        <h2 className="auth__title">Авторизация</h2>
        <hr />
        <div className="auth__links">
          <NavLink activeClassName="active" exact to="/auth/signin">Войти</NavLink>
          <NavLink activeClassName="active" exact to="/auth/signup">регистрация</NavLink>
        </div>
        <div className="auth__routers">
          <Route exact path="/auth/signin" component={SignIn} />
          <Route exact path="/auth/signup" component={SignUp} />
        </div>
        <hr />
        <div className="auth__network">
          <button>
            <img src={googleImg} alt="google" />
          </button>
          <button>
            <img src={telegramImg} alt="telegram" />
          </button>
        </div>
      </div>
    </section>
  );
}

export default Auth;
