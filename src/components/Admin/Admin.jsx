import React from "react";
import { Redirect, NavLink, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import UsersList from "./UsersList/UsersList";

const Admin = () => {
  const admin = useSelector((state) => state.auth.admin);
  if (!admin) return <Redirect to="/" />;
  return (
    <section className="admin">
      <h1 className="admin__title">Админ панель</h1>
      <div className="admin__links">
        <NavLink activeClassName="active" exact to="/admin" className="admin__link">
          Список пользователей
        </NavLink>
        <NavLink activeClassName="active" exact to="/admin/pizzas" className="admin__link">
          Добавить пиццу
        </NavLink>
      </div>
      <div className="admin__routers">
        <Route exact path="/admin" component={UsersList} />
      </div>
    </section>
  );
};

export default Admin;
