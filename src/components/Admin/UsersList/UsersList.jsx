import React from "react";

const UsersList = () => {
  return <div className="users">
      <p className="users__title">Список пользователей</p>
      <div className="users__wrapper">
          <ul className="users__list">
              <li className="users__item">
                  <img src="" alt="" />
                  <div className="users__description">
                      <p className="users__name"></p>
                      <p className="users__email"></p>
                  </div>
                  <div className="users__role">
                      <button></button>
                  </div>
              </li>
          </ul>
      </div>
  </div>;
};

export default UsersList;
