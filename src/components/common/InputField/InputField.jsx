import React from "react";
import errorImg from "../../../assets/img/errorField.svg";

const InputField = ({ input, meta, ...props }) => {
  const hasError = meta.error && meta.touched;
  return (
    <div className={hasError ? "field field__error" : "field"}>
      <input
        type="text"
        {...input}
        {...props}
        placeholder={hasError ? meta.error : ""}
      />
      <img
        src={errorImg}
        alt="error"
        className={hasError ? "field__error--img active" : "field__error--img"}
      />
    </div>
  );
};

export default InputField;
