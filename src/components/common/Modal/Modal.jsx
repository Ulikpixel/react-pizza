import React from "react";

const Modal = ({ active, children, setActive }) => {
  return (
    <>
      <div className={active ? "modal active" : "modal"}>
        <div className="modal__close" onClick={() => setActive(false)}>
          x
        </div>
        {children}
      </div>
      <div
        className={active ? "popup active" : "popup"}
        onClick={() => setActive(false)}
      ></div>
    </>
  );
}

export default Modal;
