import React from 'react';
import loader from '../../../assets/img/preloader.gif';

const Preloader = () => {
    return (
        <div className="preloader">
            <img src={loader} alt="preloader"/>
        </div>
    );
};

export default Preloader;
