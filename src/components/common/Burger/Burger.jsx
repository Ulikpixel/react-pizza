import React from "react";

const Burger = ({ active, setActive }) => {
  return (
    <div
      className={active ? "burger active" : "burger"}
      onClick={() => setActive(!active)}
    >
      <span></span>
    </div>
  );
};

export default Burger;
