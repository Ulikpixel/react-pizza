import React from "react";

const Popup = ({ active, setActive }) => {
  return (
    <div
      className={active ? "popup active" : "navbar"}
      onClick={() => setActive(false)}
    ></div>
  );
};

export default Popup;
