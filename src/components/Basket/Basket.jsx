import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getBasket, clearBasket } from "../../redux/actions/basket-actions";
import BasketBox from "./BasketBox/BasketBox";
import basketImg from "../../assets/img/basket-icon.svg";
import rubbishImg from "../../assets/img/rubbish.svg";
import BasketEmpty from "./BasketEmpty/BasketEmpty";
import Modal from "../common/Modal/Modal";

const Basket = () => {
  const dispatch = useDispatch();
  const { basket, totalPrice, totalQuantity } = useSelector(
    ({ basketPage }) => basketPage
  );
  const [modalActive, setModalActive] = useState(false);
  useEffect(() => {
    dispatch(getBasket());
  }, []);

  if (!basket.length) {
    return <BasketEmpty />;
  }
  return (
    <section className="basket">
      <div className="basket__container">
        <div className="basket__top">
          <h1 className="basket__title">
            <img src={basketImg} alt="basket" />
            Корзина
          </h1>
          <button
            className="basket__clear"
            onClick={() => dispatch(clearBasket())}
          >
            <img src={rubbishImg} alt="rubbish" />
            Очистить корзину
          </button>
        </div>
        <div className="basket__wrapper">
          {basket.map((item, i) => (
            <BasketBox {...item} key={i} />
          ))}
        </div>
        <div className="basket__score">
          <p className="basket__counter">
            Всего пицц: <span>{totalQuantity} шт.</span>
          </p>
          <p className="basket__price">
            Сумма заказа: <span>{totalPrice}₽</span>
          </p>
        </div>
        <div className="basket__btns">
          <NavLink className="basket__return" to="/">
            <svg
              width="8"
              height="14"
              viewBox="0 0 8 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M7 13L1 6.93015L6.86175 1"
                stroke="#FE5F1E"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
            Вернуться назад
          </NavLink>
          <button className="basket__pay" onClick={() => setModalActive(true)}>
            Оплатить сейчас
          </button>
          <Modal active={modalActive} setActive={setModalActive}>
            <div className="basket__order">
              <h1 className="basket__order--title">Заказ оформлен!</h1>
              <p className="basket__order--quantity">Количество пицц: <span>{totalQuantity} штук</span></p>
              <p className="basket__order--price">Общая цена: <span>{totalPrice}₽</span></p>
            </div>
          </Modal>
        </div>
      </div>
    </section>
  );
}

export default Basket;
