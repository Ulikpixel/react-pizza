import React from "react";
import emptyBasket from "../../../assets/img/empty-basket.webp";
import smile from "../../../assets/img/smile.svg";
import { NavLink } from "react-router-dom";

const BasketEmpty = () => {
  return (
    <section className="basket__empty">
      <div className="container">
        <div className="basket__empty--title">
          <h3>Корзина пустая</h3>
          <img src={smile} alt="smile"/>
        </div>
        <p className="basket__empty--warning">
          Вероятней всего, вы не заказывали ещё пиццу. Для того, чтобы заказать
          пиццу, перейди на главную страницу.
        </p>
        <img src={emptyBasket} alt="image" className="basket__empty--image" />
        <NavLink to="/" className="basket__empty--btn">
          Вернуться назад
        </NavLink>
      </div>
    </section>
  );
}

export default BasketEmpty;
