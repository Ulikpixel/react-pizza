import React from "react";
import {
  deletePizza,
  setPizza,
  minusPizza,
} from "../../../redux/actions/basket-actions";
import { useDispatch } from "react-redux";

const BasketBox = (props) => {
  const { name, imageUrl, sizes, types, quantity, price } = props;
  const dispatch = useDispatch();
  return (
    <div className="basket__wrapper--box">
      <div className="basket__wrapper--item">
        <img src={imageUrl} alt="pizza" />
        <div className="basket__wrapper--text">
          <p className="basket__wrapper--subtitle">{name}</p>
          <div className="basket__wrapper--description">
            <p className="basket__wrapper--type">{types},</p>
            <p className="basket__wrapper--size">{sizes} см.</p>
          </div>
        </div>
      </div>
      <div className="basket__wrapper--counter">
        <button
          className="basket__wrapper--minus"
          onClick={() => dispatch(minusPizza(props))}
        >
          -
        </button>
        <p className="basket__wrapper--quantity">{quantity}</p>
        <button
          className="basket__wrapper--plus"
          onClick={() => dispatch(setPizza(props))}
        >
          +
        </button>
      </div>
      <p className="basket__wrapper--price">
        <span>Цена: </span> {price} ₽
      </p>
      <button
        className="basket__wrapper--close"
        onClick={() => dispatch(deletePizza(props))}
      >
        <svg
          width="16"
          height="16"
          viewBox="0 0 16 16"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M11.7479 9.95572L9.49931 7.70712L11.7479 5.45852C12.1618 5.04459 12.1618 4.37339 11.7479 3.95946C11.334 3.54553 10.6628 3.54553 10.2488 3.95946L8.00024 6.20806L5.75164 3.95946C5.33771 3.54553 4.66651 3.54553 4.25258 3.95946C3.83865 4.37339 3.83865 5.04459 4.25258 5.45852L6.50118 7.70712L4.25258 9.95572C3.83865 10.3697 3.83865 11.0409 4.25258 11.4548C4.66651 11.8687 5.33772 11.8687 5.75164 11.4548L8.00024 9.20619L10.2488 11.4548C10.6628 11.8687 11.334 11.8687 11.7479 11.4548C12.1618 11.0409 12.1618 10.3697 11.7479 9.95572Z"
            fill="#8f8f8f"
          />
        </svg>
      </button>
    </div>
  );
}

export default BasketBox;
