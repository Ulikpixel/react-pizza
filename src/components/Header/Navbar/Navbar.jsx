import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Popup from "../../common/Popup/Popup";
import Burger from "../../common/Burger/Burger";
import { signOut } from "../../../redux/actions/auth-actions";

const Navbar = () => {
  const [modal, setModal] = useState(false);
  const { isAuth, admin } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const close = (item) => {
    item.className.includes("navbar__link") && setModal(false);
  };
  return (
    <>
      <nav
        className={modal ? "navbar active" : "navbar"}
        onClick={(e) => close(e.target)}
      >
        <ul className="navbar__list">
          <li className="navbar__item">
            <NavLink
              to="/"
              activeClassName="active"
              exact
              className="navbar__link"
            >
              Главная
            </NavLink>
          </li>
          <li className="navbar__item">
            <NavLink
              to="/basket"
              activeClassName="active"
              exact
              className="navbar__link"
            >
              Корзина
            </NavLink>
          </li>
          <li className="navbar__item">
            <NavLink
              to="/dura"
              activeClassName="active"
              exact
              className="navbar__link"
            >
              Отзыв
            </NavLink>
          </li>
          {admin && (
            <li className="navbar__item">
              <NavLink
                to="/admin"
                activeClassName="active"
                exact
                className="navbar__link"
              >
                Админ
              </NavLink>
            </li>
          )}
          {isAuth ? (
            <li className="navbar__item">
              <button
                className="navbar__link"
                onClick={() => dispatch(signOut())}
              >
                Выйти
              </button>
            </li>
          ) : (
            <li className="navbar__item">
              <NavLink
                to="/auth/signin"
                activeClassName="active"
                exact
                className="navbar__link"
              >
                Авторизация
              </NavLink>
            </li>
          )}
        </ul>
      </nav>
      <Popup active={modal} setActive={setModal} />
      <Burger active={modal} setActive={setModal} />
    </>
  );
};

export default Navbar;
