import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import {
  getPizzas,
  updateText,
  searchPizza,
  optionsItem,
} from "../../redux/actions/header-actions";
import { getBasket } from "../../redux/actions/basket-actions";
import logoImg from "../../assets/img/logo.svg";
import searchImg from "../../assets/img/search.svg";
import Modal from "../common/Modal/Modal";
import PizzasCart from "../Pizzas/PizzasCart/PizzasCart";
import NamePizzas from "./NamePizzas/NamePizzas";
import Navbar from "./Navbar/Navbar";

const Header = () => {
  const { pizzas, text, names, pizzaItem  } = useSelector((state) => state.header);
  const dispatch = useDispatch();
  const [active, setActive] = useState(false);
  const [activeModal, setActiveModal] = useState(false);
  useEffect(() => {
    dispatch(getPizzas());
    dispatch(getBasket());
  }, []);

  const showPizza = (id) => {
    dispatch(optionsItem(pizzas, id));
    setActive(false);
    setActiveModal(true);
  };
  return (
    <header className="header">
      <div className="header__logo">
        <img src={logoImg} alt="logo" />
        <div className="header__description">
          <Link to="/" className="header__title">REACT PIZZA</Link>
          <Link to="/" className="header__text">самая вкусная пицца во вселенной</Link>
        </div>
      </div>
      <div className={active ? "header__field active" : "header__field"}>
        <input
          type="text"
          placeholder="Введите текст для поиска..."
          value={text}
          onChange={(e) => {
            setActive(false);
            dispatch(updateText(e.target.value));
          }}
        />
        <button
          className="header__search"
          onClick={() => {
            dispatch(searchPizza(pizzas, text));
            setActive(!active);
          }}
          disabled={text.length ? false : true}
        >
          <img src={searchImg} alt="search" />
        </button>
        <ul className={active ? "header__list active" : "header__list"}>
          {names.length ? (
            names.map((item) => <NamePizzas {...item} showPizza={showPizza} />)
          ) : (
            <li className="header__item">Ничего не найдено</li>
          )}
        </ul>
      </div>
      <Modal active={activeModal} setActive={setActiveModal}>
        {pizzaItem.length && <PizzasCart {...pizzaItem[0]} />}
      </Modal>
      <Navbar />
    </header>
  );
}

export default Header;
