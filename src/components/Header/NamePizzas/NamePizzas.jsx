import React from "react";

const NamePizzas = ({ name, id, showPizza }) => {
  return (
    <li className="header__item" key={name} onClick={() => showPizza(id)}>
      {name}
    </li>
  );
}

export default NamePizzas;
