import React from "react";
import imageOne from "../../assets/img/image1.webp";
import imageTwo from "../../assets/img/image2.webp";
import imageThree from "../../assets/img/image3.webp";
import imageFour from "../../assets/img/image4.webp";
import imageFive from "../../assets/img/image5.webp";

const Advertising = () => {
  return (
    <section className="advertising">
      <div className="advertising__content">
        <img src={imageOne} alt="image" />
        <img src={imageTwo} alt="image" />
        <img src={imageThree} alt="image" />
        <img src={imageFour} alt="image" />
        <img src={imageFive} alt="image" />
      </div>
    </section>
  );
}

export default Advertising;
