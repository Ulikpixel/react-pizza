import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getPizzas } from "../../redux/actions/pizzas-actions";
import { getBasket } from '../../redux/actions/basket-actions';
import PizzasCart from "./PizzasCart/PizzasCart";
import Preloader from "../common/Preloader/Preloader";
import PizzasCategory from "./PizzasCategory/PizzasCategory";
import SortPopup from "./SortPopup/SortPopup";

const Pizzas = () => {
  const {
    pizzas,
    isFetching,
    sortData,
    categoryPizzas,
  } = useSelector((state) => state.pizzasPage);
  const dispatch = useDispatch();
  // category pizzas
  const [type, setType] = useState({ category: "Все", sort: "популярности" });
  // get pizzas
  useEffect(() => {
    dispatch(getPizzas(type.category, type.sort));
    dispatch(getBasket());
  }, [type]);
  return (
    <section className="pizzas">
      <div className="pizzas__nav">
        <PizzasCategory
          categoryPizzas={categoryPizzas}
          type={type}
          setType={setType}
        />
        <SortPopup sortData={sortData} type={type} setType={setType} />
      </div>
      <div className="pizzas__description">
        <h2 className="pizzas__description--title">Все пиццы</h2>
        <div className="pizzas__description--wrapper">
          {isFetching ? (
            <Preloader />
          ) : (
            pizzas.map((item, i) => (
              <PizzasCart
                {...item}
                key={i}
              />
            ))
          )}
        </div>
      </div>
    </section>
  );
}

export default Pizzas;
