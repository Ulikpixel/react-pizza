import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setPizza } from "../../../redux/actions/basket-actions";

const PizzasCart = ({
  name,
  imageUrl,
  price,
  sizes,
  types,
  id,
}) => {
  const dispatch = useDispatch();
  const basket   = useSelector((state) => state.basketPage.basket);
  const { sizePizzas, typePizzas } = useSelector(state => state.pizzasPage);

  const [activeType, setActiveType] = useState(types[0]);
  const [activeSize, setActiveSize] = useState(sizes[0]);

  const index = basket.filter((item) => item.id === id);
  const pricePizzaType = activeType === 1 ? price + 60 : price;
  const pricePizzaSize = activeSize === 26 ? 0 : activeSize;
  const totalPrice = pricePizzaType + pricePizzaSize;

  const addBasket = () => {
    const pizza = {
      id, name, imageUrl,
      startPrice: totalPrice,
      price: totalPrice,
      sizes: activeSize,
      types: typePizzas[activeType],
      quantity: 1,
    };
    dispatch(setPizza(pizza));
  };
  return (
    <div className="pizzas__description--box">
      <img src={imageUrl} alt="pizza" />
      <p className="pizzas__description--subtitle">{name}</p>
      <div className="pizzas__description--options">
        <div className="pizzas__description--type">
          {typePizzas.map((item, i) => (
            <button
              className={i === activeType ? "active" : ""}
              disabled={!types.includes(i)}
              onClick={() => setActiveType(i)}
              key={i}
            >
              {item}
            </button>
          ))}
        </div>
        <div className="pizzas__description--size">
          {sizePizzas.map((item) => (
            <button
              className={item === activeSize ? "active" : ""}
              disabled={!sizes.includes(item)}
              onClick={() => setActiveSize(item)}
              key={item}
            >
              {item} см.
            </button>
          ))}
        </div>
      </div>
      <div className="pizzas__description--order">
        <p className="pizzas__description--price">от { totalPrice } ₽</p>
        <button className="pizzas__description--add" onClick={addBasket}>
          <svg
            width="12"
            height="12"
            viewBox="0 0 12 12"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z" />
          </svg>
          <span className="pizzas__description--text">Добавить</span>
          <span
            className={
              index.length
                ? "pizzas__description--quantity active"
                : "pizzas__description--quantity"
            }
          >
            { index.reduce((acc, item) => acc + item.quantity, 0) }
          </span>
        </button>
      </div>
    </div>
  );
}

export default PizzasCart;
