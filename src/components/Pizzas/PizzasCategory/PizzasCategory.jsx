import React, { useState } from "react";

const PizzasCategory = ({ categoryPizzas, type, setType }) => {
  const [navbar, setNavbar] = useState(false);
  return (
    <>
      <ul
        className={
          navbar ? "pizzas__nav--category active" : "pizzas__nav--category"
        }
      >
        {categoryPizzas.map((item) => (
          <li
            className="pizzas__nav--item"
            key={item}
            onClick={() => setNavbar(false)}
          >
            <button
              className={
                type.category === item
                  ? "pizzas__nav--btn active"
                  : "pizzas__nav--btn"
              }
              onClick={() => setType({ ...type, category: item })}
            >
              {item}
            </button>
          </li>
        ))}
      </ul>
      <div
        className={
          navbar ? "pizzas__nav--burger active" : "pizzas__nav--burger"
        }
        onClick={() => setNavbar(!navbar)}
      >
        <span></span>
      </div>
    </>
  );
}

export default PizzasCategory;
