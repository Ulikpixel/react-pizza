import React, { useState } from "react";

const SortPopup = ({ sortData, type, setType }) => {
  const [visiblePopup, setVisiblePopup] = useState(false);
  return (
    <div className="pizzas__nav--sort">
      <button
        className={
          visiblePopup ? "pizzas__nav--toggle active" : "pizzas__nav--toggle"
        }
        onClick={() => setVisiblePopup(!visiblePopup)}
      >
        <svg
          width="10"
          height="6"
          viewBox="0 0 10 6"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M10 5C10 5.16927 9.93815 5.31576 9.81445 5.43945C9.69075 5.56315 9.54427 5.625 9.375 5.625H0.625C0.455729 5.625 0.309245 5.56315 0.185547 5.43945C0.061849 5.31576 0 5.16927 0 5C0 4.83073 0.061849 4.68424 0.185547 4.56055L4.56055 0.185547C4.68424 0.061849 4.83073 0 5 0C5.16927 0 5.31576 0.061849 5.43945 0.185547L9.81445 4.56055C9.93815 4.68424 10 4.83073 10 5Z"
            fill="#2C2C2C"
          />
        </svg>
      </button>
      <p className="pizzas__nav--description">Сортировка по:</p>
      <div className="pizzas__nav--choice">
        <p
          className="pizzas__nav--result"
          onClick={() => setVisiblePopup(!visiblePopup)}
        >
          {type.sort}
        </p>
        <ul
          className={
            visiblePopup ? "pizzas__nav--modal active" : "pizzas__nav--modal"
          }
        >
          {sortData.map((el) => {
            return (
              <li
                key={el}
                className={
                  type.sort === el
                    ? "pizzas__nav--type active"
                    : "pizzas__nav--type"
                }
                onClick={() => setType({ ...type, sort: el })}
              >
                {el}
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}

export default SortPopup;
