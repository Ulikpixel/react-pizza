export { default as Header } from './Header/Header';
export { default as Advertising } from './Advertising/Advertising';
export { default as Pizzas } from './Pizzas/Pizzas';
export { default as Basket } from './Basket/Basket';
export { default as Auth } from './Auth/Auth';
export { default as Admin } from "./Admin/Admin";
export { default as Feedback } from "./Feedback/Feedback";

