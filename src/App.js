import { Route } from "react-router-dom";
import { Header, Advertising, Pizzas, Basket, Auth, Admin, Feedback } from './components';

const App = () => {
  return (
    <div className="app">
      <div className="container">
        <Header />
        <main className="app__content">
          <Route exact path="/" component={Advertising} />
          <Route exact path="/" component={Pizzas} />
          <Route exact path="/basket" component={Basket} />
          <Route path="/auth" component={Auth} />
          <Route path="/admin" component={Admin} />
          <Route path="/feedback" component={Feedback} />
        </main>
      </div>
    </div>
  );
};

export default App;
