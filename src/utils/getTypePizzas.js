export const getTypePizzas = (type, arr) => {
    switch (type) {
        case "Все":
            return arr;
        case "Мясные":
            return arr.filter(item => item.category.includes("Мясные"));
        case "Вегетарианская":
            return arr.filter(item => item.category.includes("Вегетарианская"));
        case "Гриль":
            return arr.filter(item => item.category.includes("Гриль"));
        case "Острые":
            return arr.filter(item => item.category.includes("Острые"));
        default:
            return arr;
    };
};
