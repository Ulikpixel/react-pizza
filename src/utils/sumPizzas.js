export const sumPizzas = (arr, pizza, sum) => {
    const obj = arr.filter(el => el.id === pizza.id && el.sizes === pizza.sizes && el.types === pizza.types)[0];
    if (sum === 'plus') {
        return obj ? { ...obj, quantity: obj.quantity + 1, price: obj.startPrice * (obj.quantity + 1) } : pizza;
    }
    if (sum === 'minus') {
        return obj ? { ...obj, quantity: obj.quantity - 1, price: obj.startPrice * (obj.quantity - 1) } : pizza;
    }
};
