import { sumPizzas } from './sumPizzas';
import { sortPizzas } from './sortPizzas';

export const getPizzaLocal = () => {
    const data = localStorage.getItem('basket') || '[]';
    const result = JSON.parse(data);
    return result;
};

export const deletePizzaLocal = (pizza) => {
    const pizzas = getPizzaLocal();
    const newPizzas = pizzas.filter(el => el.id !== pizza.id || el.sizes !== pizza.sizes || el.types !== pizza.types);
    localStorage.setItem('basket', JSON.stringify(newPizzas));
};

export const setPizzaLocal = (pizza, sum = 'plus') => {
    const pizzas = getPizzaLocal();
    const pizzaItem = sumPizzas(pizzas, pizza, sum);
    const newPizza = pizzaItem.quantity <= 0 ? { ...pizzaItem, quantity: 1 } : { ...pizzaItem };
    const newPizzas = pizzas.filter(el => el.id !== pizza.id || el.sizes !== pizza.sizes || el.types !== pizza.types);
    const result = sortPizzas("по цене", [{ ...newPizza }, ...newPizzas]);
    localStorage.setItem('basket', JSON.stringify(result));
};


