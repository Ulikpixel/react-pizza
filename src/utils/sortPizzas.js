import { sortAlphabet } from './sortAlphabet';

export const sortPizzas = (sort, arr) => {
    switch (sort) {
        case "популярности":
            return arr.sort((a, b) => a.rating - b.rating).reverse();
        case "по цене":
            return arr.sort((a, b) => a.price - b.price).reverse();
        case "по алфавиту":
            return sortAlphabet(arr, 'name');
        default:
            return arr;
    };
};

