export const SET_PIZZAS  = "SET_PIZZAS";
export const IS_FETCHING = "IS_FETCHING";

const initialState = {
    pizzas: [],
    isFetching: false,
    sortData: ["популярности", "по цене", "по алфавиту"],
    categoryPizzas: ["Все", "Мясные", "Вегетарианская", "Гриль", "Острые"],
    sizePizzas: [26, 30, 40],
    typePizzas: ["тонкое", "традиционное"],
};

const pizzasReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_PIZZAS:
            return { ...state, pizzas: action.pizzas }; 
        case IS_FETCHING:
            return { ...state, isFetching: action.fetching };
        default:
            return state;
    };
};

export default pizzasReducer;