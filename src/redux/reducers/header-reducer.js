export const SET_PIZZAS     = "SET_PIZZAS";
export const UPDATE_TEXT    = "UPDATE_TEXT";
export const SET_NAMES      = "SET_NAMES";
export const SET_PIZZA_ITEM = "SET_PIZZA_ITEM";

const initialState = {
    pizzas: [],
    text: '',
    names: [],
    pizzaItem: [],
};

const headerReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_PIZZAS:
            return { ...state, pizzas: action.pizzas };
        case UPDATE_TEXT:
            return { ...state, text: action.text };
        case SET_NAMES:
            return { ...state, names: action.names };
        case SET_PIZZA_ITEM:
            return { ...state, pizzaItem: action.pizza } ;
        default:
            return state;
    };
};

export default headerReducer;