const initialState = {
    feedbackList: [],
};

const feedbackReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    };
};

export default feedbackReducer;