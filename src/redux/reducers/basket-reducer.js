export const SET_BASKET = "SET_BASKET";
export const SET_TOTAL_PRICE = "SET_TOTAL_PRICE";
export const SET_TOTAL_QUANTITY = "SET_TOTAL_QUANTITY";

const initialState = {
    basket: [],
    totalPrice: 0,
    totalQuantity: 0,
};

const basketReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_BASKET:
            return { ...state, basket: action.basket };
        case SET_TOTAL_PRICE:
            return { ...state, totalPrice: action.price };
        case SET_TOTAL_QUANTITY:
            return { ...state, totalQuantity: action.quantity };
        default:
            return state;
    };
};

export default basketReducer;