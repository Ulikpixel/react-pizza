export const SET_AUTH = "SET_AUTH";
export const SET_USER = "SET_USER";
export const LOGIN_PROGRESS = "LOGIN_PROGRESS";
export const SET_ADMIN = "SET_ADMIN";
export const SET_MANAGER = "SET_MANAGER";
export const LOGOUT = "LOGOUT";

const initialState = {
    isAuth: false,
    loginProgress: false,
    admin: false,
    user: {},
    manager: false,
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_AUTH:
            return { ...state, isAuth: action.isAuth };
        case SET_USER:
            return { ...state, user: action.payload };
        case LOGIN_PROGRESS:
            return { ...state, loginProgress: action.progress };
        case SET_ADMIN:
            return { ...state, admin: action.isAdmin };
        case SET_MANAGER:
            return { ...state, manager: action.isManager };
        case LOGOUT:
            return { 
                ...state, isAuth: false, 
                loginProgress: false, 
                admin: false, user: {}, 
                manager: false }
            ;
        default:
            return state;
    };
};

export default authReducer;