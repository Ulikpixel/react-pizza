import { SET_BASKET, SET_TOTAL_PRICE, SET_TOTAL_QUANTITY } from '../reducers/basket-reducer';
import { getPizzaLocal, setPizzaLocal, deletePizzaLocal } from '../../utils/localStorage';

// actions
const setBasket = (basket) => ({ type: SET_BASKET, basket });
const setTotalPrice = (price) => ({ type: SET_TOTAL_PRICE, price });
const setTotalQuantity = (quantity) => ({ type: SET_TOTAL_QUANTITY, quantity });

// thunk
export const getBasket = () => (dispatch) => {
    const pizzas = getPizzaLocal();
    const totalPrice = pizzas.reduce((acc, item, i) => acc + item.price, 0);
    const totalQuantity = pizzas.reduce((acc, item) => acc + item.quantity, 0);
    dispatch(setBasket(pizzas));
    dispatch(setTotalPrice(totalPrice));
    dispatch(setTotalQuantity(totalQuantity));
};

export const setPizza = (pizza) => (dispatch) => {
    setPizzaLocal(pizza, 'plus');
    dispatch(getBasket());
};

export const clearBasket = () => (dispatch) => {
    localStorage.clear();
    dispatch(getBasket());
};

export const deletePizza = (pizza) => (dispatch) => {
    deletePizzaLocal(pizza);
    dispatch(getBasket());
};

export const minusPizza = (pizza) => (dispatch) => {
    setPizzaLocal(pizza, 'minus');
    dispatch(getBasket());
};



