import { SET_PIZZAS, IS_FETCHING } from '../reducers/pizzas-reducer';
import { pizzasAPI } from '../../API/pizzasAPI';

const setPizzas = (pizzas) => ({ type: SET_PIZZAS, pizzas });
const isFetching = (fetching) => ({ type: IS_FETCHING, fetching });

export const getPizzas = (category = "Все", sort = "популярности") => (dispatch) => {
    dispatch(isFetching(true));
    pizzasAPI.get(category, sort).then(data => {
        dispatch(setPizzas(data));
        dispatch(isFetching(false));
    });
};


