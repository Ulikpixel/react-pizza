import { SET_PIZZAS, UPDATE_TEXT, SET_NAMES, SET_PIZZA_ITEM } from '../reducers/header-reducer';
import { pizzasAPI } from '../../API/pizzasAPI';
import { searchItem } from "../../utils/searchItem";

//actions
const setPizzas = (pizzas) => ({ type: SET_PIZZAS, pizzas });
export const updateText = (text) => ({ type: UPDATE_TEXT, text });
export const setItemsNames = (names) => ({ type: SET_NAMES, names });
export const setItemPizza = (pizza) => ({ type: SET_PIZZA_ITEM, pizza });

// thunks
export const getPizzas = () => (dispatch) => {
    pizzasAPI.get().then(data => {
        dispatch(setPizzas(data));
    });
};

export const searchPizza = (pizzas, text) => (dispatch) => {
    const result = searchItem(pizzas)("name", text);
    dispatch(setItemsNames(result));
};

export const optionsItem = (pizzas, id) => (dispatch) => {
    const pizza = pizzas.filter((item) => item.id === id);
    dispatch(updateText(""));
    dispatch(setItemPizza(pizza));
};