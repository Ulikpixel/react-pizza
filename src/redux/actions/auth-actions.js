import { SET_AUTH, SET_USER, LOGIN_PROGRESS, SET_ADMIN, SET_MANAGER, LOGOUT } from '../reducers/auth-reducer';
import { authAPI } from "../../API/authAPI";
import { stopSubmit } from 'redux-form';

const setAuth = (isAuth) => ({ type: SET_AUTH, isAuth });
const setUser = (payload) => ({ type: SET_USER, payload });
const loginProgress = (progress) => ({ type: LOGIN_PROGRESS, progress });
const setAdmin = (isAdmin) => ({ type: SET_ADMIN, isAdmin });
const setManager = (isManager) => ({ type: SET_MANAGER, isManager });
const logout = () => ({ type: LOGOUT });

export const signUp = (data) => (dispatch) => {
    const { email, password, name, surname } = data;
    dispatch(loginProgress(true));
    authAPI.signUp(email, password).then(cred => {
        dispatch(loginProgress(false));
        dispatch(setAuth(true));
        const id = cred.user.uid;
        const user = {
            name: name,
            surname: surname, email: email,
            role: "user", id: id };
        authAPI.setUser(id, user);
        dispatch(setUser(user));
    }).catch((e) => {
        dispatch(loginProgress(false));
        dispatch(stopSubmit('signup', { _error: "Вы не правильно ввели свой email или пароль!" }));
    });
};

export const signIn = (email, password) => (dispatch) => {
    dispatch(loginProgress(true));
    authAPI.signIn(email, password).then((cred) => {
        dispatch(loginProgress(false));
        dispatch(setAuth(true));
        const id = cred.user.uid;
        authAPI.getUser(id).then(data => {
            dispatch(setUser(data))
            if (data.role === "admin") {
                dispatch(setAdmin(true));
            } else if (data.role === "manager") {
                dispatch(setManager(true));
            }
        });
    }).catch((e) => {
        dispatch(loginProgress(false));
        dispatch(stopSubmit('signin', { _error: "Вы не правильно ввели свой email или пароль!" }));
    });
};

export const signOut = () => (dispatch) => {
    authAPI.signOut();
    dispatch(logout());
};

