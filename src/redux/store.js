import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import headerReducer from './reducers/header-reducer';
import pizzasReducer from './reducers/pizzas-reducer';
import basketReducer from './reducers/basket-reducer';
import authReducer from './reducers/auth-reducer';
import feedbackReducer from "./reducers/feedback-reducer";
import { reducer as formReducer } from 'redux-form';

const reducers = combineReducers({
    header: headerReducer,
    pizzasPage: pizzasReducer,
    basketPage: basketReducer,
    auth: authReducer,
    form: formReducer,
    feedback: feedbackReducer,
});

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

window.store = store;
export default store;