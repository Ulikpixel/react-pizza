import axios from 'axios';
import { getTypePizzas } from '../utils/getTypePizzas';
import { sortPizzas } from '../utils/sortPizzas';

const instance = axios.create({
    baseURL: "https://react-pizza-88bdd-default-rtdb.firebaseio.com/",
    headers: {
        'API-KEY': "AIzaSyDRyGE8hxXS6V92dGj6Bx37a0Yj4-j3mkc"
    },
});

export const pizzasAPI = {
    async get(category, sort) {
        const response = await instance.get('pizzas.json');
        const pizzas = Object.keys(response.data).map((key) => {
            return { ...response.data[key] }
        });
        const typePizzas = getTypePizzas(category, pizzas);
        const result = sortPizzas(sort, typePizzas);
        return result;
    },
    async post(body) {
        instance.post("pizzas.json", body);
    },
};
