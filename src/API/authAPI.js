import { authFire, database } from '../firebase';
import axios from 'axios';

const instance = axios.create({
    baseURL: "https://react-pizza-88bdd-default-rtdb.firebaseio.com/",
    headers: {
        'API-KEY': "AIzaSyDRyGE8hxXS6V92dGj6Bx37a0Yj4-j3mkc"
    },
});

export const authAPI = {
    signUp(email, password) {
        return authFire.createUserWithEmailAndPassword(email, password);
    },
    signIn(email, password) {
        return authFire.signInWithEmailAndPassword(email, password);
    },
    signOut() {
        authFire.signOut();
    },
    setUser(id, payload) {
        return database.ref("/users/" + id).set(payload);
    },
    async getUser(id){
        const response = await instance.get(`users/${id}.json`);
        return response.data;
    },
};

