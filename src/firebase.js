import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyDRyGE8hxXS6V92dGj6Bx37a0Yj4-j3mkc",
    authDomain: "react-pizza-88bdd.firebaseapp.com",
    databaseURL: "https://react-pizza-88bdd-default-rtdb.firebaseio.com",
    projectId: "react-pizza-88bdd",
    storageBucket: "react-pizza-88bdd.appspot.com",
    messagingSenderId: "106469487731",
    appId: "1:106469487731:web:f2bfa4ecadab5057fba606",
});

export const authFire = firebase.auth();
export const database = firebase.database();

export default firebase;